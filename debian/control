Source: google-auth-httplib2
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Valentin Vidic <vvidic@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-google-auth,
 python3-httplib2 (>= 0.19.0),
 python3-pytest,
 python3-pytest-flask,
 python3-pytest-localserver,
 python3-setuptools,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python
Homepage: https://github.com/GoogleCloudPlatform/google-auth-library-python-httplib2
Vcs-Git: https://salsa.debian.org/python-team/packages/google-auth-httplib2.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/google-auth-httplib2

Package: python3-google-auth-httplib2
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description: Google Authentication Library: httplib2 transport
 Python library providing a httplib2 transport for google-auth.
 .
 Note that httplib has lots of problems such as lack of thread safety and
 insecure usage of TLS. Using it is highly discouraged.
 .
 This library is intended to help existing users of oauth2client migrate
 to google-auth.
